<?php 
session_start();
if(isset($_SESSION['compte'])){ 
?>
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Delete my account</title>
    <link rel="stylesheet" href="css/delete.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400&display=swap" rel="stylesheet">
</head>
<body>
    <button class="btn-history">
        <a href="client.php" class="a-history white"><i class="fa-solid fa-arrow-left"></i></a>
    </button>
    <div class="logo">
        <img src="stock/ress/logoDiet.png" class="img-logo" alt="logo">    
    </div>
    <h1 style="text-align:center">Are you sure you want delete your account ?</h1>
    <p class="warning">Warning: after the deletion of the account you could not recover it, any deletion is final</p>
    <form action="./controller/delete.php" method="post">
        <label class="white" style="margin:10px;"><i class="fa-regular fa-user"></i>Username : </label>
        <input class="input-update" type="text" placeholder="Type your username to delete your account" name="username" value="">
        <div class="div-submit">
          <input type="submit" class="input-submit" value="submit">   
        </div>
           
    </form>
    <script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
</body>
</html>
<?php }else{
    header('location:./register-login.php');
}
?>