<?php 
session_start();
include './model/data.php';
$tab = [
    'maigre' => ['c1' => 'rgba(15, 0, 108, 1)','c2' => 'rgba(1, 178, 236, 1)','img' => 'maigre.png','phrase-1' => 'Your bmi indicates that you are skinny','phrase-2'=>' to reach your normal weight go above 18.5 and below 25'],
    'normal' => ['c1' => 'rgba(29, 89, 0, 1)','c2' => 'rgba(120, 184, 42, 1)','img' => 'normal.png','phrase-1' => 'Your BMI indicates that you are normal','phrase-2'=>' stay between 18.5 and 25'],
    'surpoids' => ['c1' => 'rgba(148, 53, 0, 1)','c2' => 'rgba(245, 159, 0, 1)','img' => 'surpoids.png','phrase-1' => 'Your BMI indicates that you are overweight','phrase-2'=>' to reach your normal weight, go below 25 and above 18.5'],
    'obesite' => ['c1' => 'rgba(194, 2, 2, 1)','c2' => 'rgba(238, 112, 2, 1)','img' => 'obesite.png','phrase-1' => 'Your BMI indicates that you are obese','phrase-2'=>' to reach your normal weight, go below 25 and above 18.5'],
    'morbide' => ['c1' => 'rgba(0, 0, 0, 1)','c2' => 'rgba(170, 170, 170, 1)','img' => 'morbide.png','phrase-1' => 'Your BMI indicates that you are morbidly obese','phrase-2'=>' to reach your normal weight, go below 25 and above 18.5']
    ];
if(isset($_SESSION['compte'])){
if(verif($_SESSION['compte']['id']) === true && SelectAll($_SESSION['compte']['id'])[0]['date'] === date('Y-m-d')){
    $imc = SelectAll($_SESSION['compte']['id'])[0]['imc'];
    if($imc <= 18.5){
        $cat = 'maigre';
    }
    if($imc > 18.5 && $imc <= 25){
        $cat = 'normal';
    }
    if($imc > 25 && $imc <= 30){
        $cat = 'surpoids';
    }
    if($imc > 30 && $imc <= 40){
        $cat = 'obesite';
    }
    if($imc >= 40){
        $cat = 'morbide';
    }
    $couleur1 = $tab[$cat]['c1'];
    $couleur2 = $tab[$cat]['c2'];
    $img = $tab[$cat]['img'];
    $phrase1 = $tab[$cat]['phrase-1'];
    $phrase2 = $tab[$cat]['phrase-2'];
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espace Client</title>
    <link rel="stylesheet" href="css/client.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400&display=swap" rel="stylesheet">
    
</head>
<body>
    <?php if(verif($_SESSION['compte']['id']) === true && SelectAll($_SESSION['compte']['id'])[0]['date'] === date('Y-m-d')){?>
        <header>
            <img src="stock/ress/logoDiet.png" class="logo-header" alt='logo du site'>
            <a href="index.php" class="white">Home</a>
            <a href="destroy.php" class="white"><i class="fa-solid fa-right-from-bracket"></i></a>
        </header>
        <div class="profil" style="border-bottom: <?php echo $couleur1 ?> 3px solid;">
            <img src="<?php echo SelectUser($_SESSION['compte']['id'])[0]['photo_profil']?>" class="profil-picture" alt='user picture'>
            <div class="name">
                <p class="firstname"><?php echo SelectUser($_SESSION['compte']['id'])[0]['firstname']?></p>
                <p class="lastname"><?php echo SelectUser($_SESSION['compte']['id'])[0]['lastname']?></p>
            </div>
        </div>
        <div class="imc" style="background:<?php echo $couleur1 ?>">
            <img src="<?php echo './stock/ress/'.$img ?>" class="img-imc" alt='picture of stickman'>
            <div class="phrase">
                <p class="info-imc white"><?php echo $phrase1 ?></p>
                <p class="more-info-imc white">you currently have a BMI = <strong><?php echo intval(SelectAll($_SESSION['compte']['id'])[0]['imc']).'</strong>'.$phrase2 ?></p>
            </div>
        </div>
        <div class="cal" style="background:<?php echo $couleur2 ?>">
            <img src="./stock/ress/cal.png" class="img-cal" alt='logo calorie'>
            <p class=" cal-phrase">You have to eat <strong><?php echo intval(SelectAll($_SESSION['compte']['id'])[0]['cal']) ?></strong> Kcal Today</p>
        </div>
        <div class="more-info">
            <table>
                <thead>
                    <tr>
                        <th>gender</th>
                        <th>age</th>
                        <th>height</th>
                        <th>weight</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php if(SelectAll($_SESSION['compte']['id'])[0]['genre'] === 1){echo 'Female';}else{echo 'Male';} ?></td>
                        <td><?php echo SelectAll($_SESSION['compte']['id'])[0]['age'] ?></td>
                        <td><?php echo SelectAll($_SESSION['compte']['id'])[0]['height'] ?>m</td>
                        <td><?php echo SelectAll($_SESSION['compte']['id'])[0]['weight'] ?>Kg</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="div-history">
            <a href="historique.php" class="a-history white btn-history">See my History</a>
            <a href="delete.php" class="a-history white btn-history">Delete my account</a>
        </div>
        <a href="tel:123-456-7890" class="callback a-history">Call us if you want to go further in your diet</a>
        <form class="update-user" style="background:<?php echo $couleur1 ?>" action="./controller/update.php" method="post" enctype="multipart/form-data">
            <p class="warning">Warning : this form only modifies today's information</p>    
            <label class="white">Change my personnal information : </label>
            <label for="picture" class="picture" id="label"><img id='picture-preview' src="./<?php echo SelectUser($_SESSION['compte']['id'])[0]['photo_profil']?>" style='width:100px;height:100px;border-radius: 50%;' alt="update user picture"></label>
            <input type="file" id="picture" onchange="previewPicture(this)" name="picture">
            <label class="white"><i class="fa-regular fa-user"></i>Username : </label>
            <input class="input-update" type="text" placeholder="8 - 20 character" name="username" value="<?php echo SelectUser($_SESSION['compte']['id'])[0]['pseudo']?>">
            <label class="white"><i class="fa-solid fa-at"></i>Mail : </label>
            <input class="input-update" type="email" placeholder="Exemple@mail.com" name="email" value="<?php echo SelectUser($_SESSION['compte']['id'])[0]['mail']?>">
            <label class="white" for="birth"><i class="fa-regular fa-calendar"></i>Birth : </label>
            <input class="input-update" type="date" id="birth" name="date" min="1920-01-01" max="2012-12-31" value="<?php echo SelectUser($_SESSION['compte']['id'])[0]['birth']?>">
            <label class="white-label" for="weight" id="1"></label>
            <input type="range" name="weight" id="weight" class="input-first" onchange="change(this,'1')" min="20" max="300" value="<?php echo SelectAll($_SESSION['compte']['id'])[0]['weight']?>">
            <label class="white-label" for='height' id="2" ></label>
            <input type="range" name="height" id="height" class="input-first input-update" min="0.50" max="3" step="0.01" value="<?php echo SelectAll($_SESSION['compte']['id'])[0]['height']?>">
            <select class="select-first" name="profil">
            <option value="<?php echo SelectAll($_SESSION['compte']['id'])[0]['activite'] ?>">Change my profil</option>
                <option value="1.2">low sports expenditure (sedentary profile)</option>
                <option value="1.375">train 1 to 3 times a week (lightly active profile)</option>
                <option value="1.55">train 4 to 6 times a week (active profile)</option>
                <option value="1.725">daily sports or intense physical exercises (very active profile)</option>
            </select>
            <input type="submit" class="input-submit" value="confirm">
            
        </form>
    <?php }else if(verif($_SESSION['compte']['id']) === false){ ?>
        <form class="form-first-time" method="post" action="./controller/insert.php" enctype="multipart/form-data">
            <label for="picture" class="picture" id="label">add new photo</label>
            <input type="file" id="picture" onchange="previewPicture(this)" name="picture">
            <label class="white-label" for="weight" id="1"></label>
            <input type="range" name="weight" id="weight" class="input-first" onchange="change(this,'1')" min="20" max="300" value="65">
            <label class="white-label" for='height' id="2" ></label>
            <input type="range" name="height" id="height" class="input-first" min="0.50" max="3" step="0.01" value="1.00">
            <select class="select-first"  name="profil">
            <option value="" >Your Profile</option>
                <option value="1.2">low sports expenditure (sedentary profile)</option>
                <option value="1.375">train 1 to 3 times a week (lightly active profile)</option>
                <option value="1.55">train 4 to 6 times a week (active profile)</option>
                <option value="1.725">daily sports or intense physical exercises (very active profile)</option>
            </select>
            <input type="submit" class="input-submit" value="confirm">
            
        </form>
    <?php }else if(verif($_SESSION['compte']['id']) === true && SelectAll($_SESSION['compte']['id'])[0]['date'] !== date('Y-m-d')){ ?>
        
        <form class="form-first-time" method="post" action="./controller/insert.php" enctype="multipart/form-data">
            <label class="white" style="font-size:x-large;">Daily report</label>
            <label for="picture" class="picture" id="label"><img id='picture-preview' src="./<?php echo SelectUser($_SESSION['compte']['id'])[0]['photo_profil']?>" style='width:100px;height:100px;border-radius: 50%;'></label>
            <input type="file" id="picture" onchange="previewPicture(this)" name="picture">
            <label class="white-label" for="weight" id="1"></label>
            <input type="range" name="weight" id="weight" class="input-first" onchange="change(this,'1')" min="20" max="300" value="<?php echo SelectAll($_SESSION['compte']['id'])[0]['weight']?>">
            <label class="white-label" for='height' id="2" ></label>
            <input type="range" name="height" id="height" class="input-first" min="0.50" max="3" step="0.01" value="<?php echo SelectAll($_SESSION['compte']['id'])[0]['height']?>">
            <select class="select-first"  name="profil">
            <option value="<?php echo SelectAll($_SESSION['compte']['id'])[0]['activite']; ?>" >Change my profil</option>
                <option value="1.2">low sports expenditure (sedentary profile)</option>
                <option value="1.375">train 1 to 3 times a week (lightly active profile)</option>
                <option value="1.55">train 4 to 6 times a week (active profile)</option>
                <option value="1.725">daily sports or intense physical exercises (very active profile)</option>
            </select>
            <input type="submit" class="input-submit" value="confirm">
            
        </form>
        
    
    <?php }}else{header('location:./register-login.php'); }
     ?>
     <script>
            document.getElementById('1').innerHTML = `Your weight : ${document.getElementById('weight').value}kg`
            document.getElementById('2').innerHTML = `Your height : ${document.getElementById('height').value}m`
            document.getElementById('weight').addEventListener('input', function(){
                document.getElementById('1').innerHTML = `Your weight : ${document.getElementById('weight').value}kg`
            })
            document.getElementById('height').addEventListener('input', function(){
                document.getElementById('2').innerHTML = `Your height : ${document.getElementById('height').value}m`
            })
        var previewPicture = function (e) {
            // e.files contient un objet FileList
            document.getElementById('label').innerHTML = "<img id='picture-preview' style='width:100px;height:100px;border-radius: 50%;'>"
            const [picture] = e.files;
            // "picture" est un objet File
            if (picture) {
                // L'objet FileReader
                var reader = new FileReader();

                // L'événement déclenché lorsque la lecture est complète
                reader.onload = function (e) {
                // On change l'URL de l'image (base64)
                document.getElementById('picture-preview').src = e.target.result;
                };

                // On lit le fichier "picture" uploadé
                reader.readAsDataURL(picture);
            }
        };
        </script>
        <script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
</body>

</html>