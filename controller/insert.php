<?php 
session_start();
include '../model/data.php';
if(
    $_POST['weight'] !== 0 && $_POST['height'] !== 0 && $_POST['profil'] !== ""
){  
    $id_user = SelectUser($_SESSION['compte']['id'])[0]['id'];
    $imc = CalculIMC($_POST['weight'],$_POST['height']); 
    $age = date('Y-m-d') - SelectUser($id_user)[0]['birth'];
    $gender = SelectUser($id_user)[0]['genre'];
    if($gender !== "0"){
        $calorie = ( ( (9.740 * floatval($_POST['weight']) ) + (172.9 * floatval($_POST['height']) ) - (4.737 * $age) + 667.051) * floatval($_POST['profil'])); 
    }else{
        $calorie = ( ( (13.707 * floatval($_POST['weight']) ) + (492.3 * floatval($_POST['height']) ) - (6.673 * $age) + 77.607) * floatval($_POST['profil']));  
    }
    
    $id_imc = NULL;
    $id_imc = addInfo($imc,intval($calorie),floatval($_POST['weight']),floatval($_POST['height']),floatval($_POST['profil']),$age);
    if($id_imc !== NULL){
        liaison($id_imc,$id_user);
    }
    if (move_uploaded_file($_FILES['picture']['tmp_name'], "../stock/uploads/".$_FILES['picture']['name'])) {
        print "Téléchargé avec succès!";
        addPicture("stock/uploads/".$_FILES['picture']['name'],$_SESSION['compte']['id']);
    } else {
        print "Échec du téléchargement!";
    }
    header('location:../client.php');
}else{
    header('location:../client.php');
}