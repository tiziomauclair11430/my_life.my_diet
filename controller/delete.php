<?php 
session_start();
include '../model/data.php';
$ancienUser = SelectUser($_SESSION['compte']['id'])[0];
if($_POST['username'] !== "" && $_POST['username'] === $ancienUser['pseudo']){
    unlink('../'.$ancienUser['photo_profil']);
    deleteClientInfo($_SESSION['compte']['id']);
    deleteClient($_SESSION['compte']['id']);
    session_destroy();
    header('location:../register-login.php');
}else{
    header('location:../delete.php#err='.true);
}
?>