<?php 
session_destroy();
session_start();
include '../model/data.php';
if( $_POST['username'] !== "" &&
    $_POST['email'] !== "" &&
    $_POST['fname'] !== "" &&
    $_POST['lname'] !== "" &&
    $_POST['date'] !== "" &&
    $_POST['gender'] !== "" &&
    $_POST['password'] !== ""
){
    $id = addUser($_POST['username'],$_POST['email'],$_POST['fname'],$_POST['lname'],$_POST['date'],intval($_POST['gender']),$_POST['password']);
    $_SESSION["compte"] = ["pseudo" => $_POST['username'],"client" => true,'id' => $id];
    header('location:../client.php');
}
header('location:../register-login.php');
?>