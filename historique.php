<?php session_start();
include './model/data.php'; 
if(isset($_SESSION['compte']) && SelectAll($_SESSION['compte']['id'])[0]['date'] === date('Y-m-d')){
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>History</title>
    <link rel="stylesheet" href="css/history.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.9.1/chart.min.js" integrity="sha512-ElRFoEQdI5Ht6kZvyzXhYG9NqjtkmlkfYk0wr6wHxU9JEHakS7UJZNeml5ALk+8IKlU6jDgMabC3vkumRokgJA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>
<body>
    <header>
        <img src="stock/ress/logoDiet.png" class="logo-header">
        <a href="index.php" class="white">Home</a>
        <a href="destroy.php" class="white"><i class="fa-solid fa-right-from-bracket"></i></a>
    </header>
    <div class="profil" style="border-bottom: <?php echo $couleur1 ?> 3px solid;">
        <img src="<?php echo SelectUser($_SESSION['compte']['id'])[0]['photo_profil']?>" class="profil-picture">
        <div class="name">
            <p class="firstname"><?php echo SelectUser($_SESSION['compte']['id'])[0]['firstname']?></p>
            <p class="lastname"><?php echo SelectUser($_SESSION['compte']['id'])[0]['lastname']?></p>
        </div>
    </div>
    <div class="main-heading">
        <button class="btn-history">
            <a href="client.php" class="a-history white"><i class="fa-solid fa-arrow-left"></i></a>
        </button>
        <p class="title">History</p>
    </div>
    <?php 
    $data1 = '';
    $data2 = '';
    $data3 = '';
    foreach(SelectAllInfo($_SESSION['compte']['id']) as $select){ 
        $data1 = $data1 . '"'. $select['date'].'",';
        $data2 = $data2 . '"'. $select['imc'] .'",';
        $data3 = $data3 . '"'. $select['cal'] .'",';     
    ?>
        <div class="card-history">
            <table>
                <thead>
                    <tr>
                        <th>bmi</th>
                        <th>age</th>
                        <th>height</th>
                        <th>weight</th>
                        <th>Kcal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td name="imc"><?php echo $select['imc'] ?></td>
                        <td><?php echo $select['age'] ?></td>
                        <td><?php echo $select['height'] ?>m</td>
                        <td><?php echo $select['weight'] ?>Kg</td>
                        <td><?php echo $select['cal'] ?>Kcal</td>
                    </tr>
                </tbody>
            </table>
            <div class="date-color">
                <p><?php echo $select['date'] ?></p>
                <span class="color" name="span"></span>
            </div>
        </div>
    <?php }
    $data1 = trim($data1,",");
    $data2 = trim($data2,",");
    ?>    
        <div class="container"> 

            <canvas id="chart" style="width: 100%; height: 68vh; background: #222; border: 1px solid #555652; margin-top: 10px;"></canvas>
            
            <script>
                var ctx = document.getElementById("chart").getContext('2d');
                var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [<?php echo $data1; ?>],
                    datasets: 
                    [
                    {
                        label: 'IMC ',
                        data: [<?php echo $data2; ?>,],
                        backgroundColor: 'transparent',
                        borderColor:'rgba(0,255,255)',
                        borderWidth: 3  
                    },
                    {
                        label: 'Kcal ',
                        data: [<?php echo $data3; ?>,],
                        backgroundColor: 'transparent',
                        borderColor:'rgba(255,0,255)',
                        borderWidth: 3  
                    }]
                },

                options: {
                    scales: {scales:{yAxes: [{beginAtZero: false}], xAxes: [{autoskip: true, maxTicketsLimit: 20}]}},
                    tooltips:{mode: 'index'},
                    legend:{display: true, position: 'top', labels: {fontColor: 'rgb(255,255,255)', fontSize: 16}}
                }
            });
            </script>
        </div>
        <script>
            const HtmlImc = document.getElementsByName('imc') 
            const Htmlspan = document.getElementsByName('span') 
            console.log(HtmlImc.length)
            for(let i = 0;i <= HtmlImc.length; i++){
                if(HtmlImc[i].textContent <= 18.5){
            
                    Htmlspan[i].style.backgroundColor = "rgba(1, 178, 236, 1)"
                }
                if(HtmlImc[i].textContent > 18.5 && HtmlImc[i].textContent <= 25){
                    
                    Htmlspan[i].style.backgroundColor = "rgba(120, 184, 42, 1)"
                }
                if(HtmlImc[i].textContent > 25 && HtmlImc[i].textContent <= 30){
                    
                    Htmlspan[i].style.backgroundColor = "rgba(245, 159, 0, 1)"
                }
                if(HtmlImc[i].textContent > 30 && HtmlImc[i].textContent <= 40){
                    
                    Htmlspan[i].style.backgroundColor = "rgba(238, 112, 2, 1)"
                }
                if(HtmlImc[i].textContent >= 40){
                    
                    Htmlspan[i].style.backgroundColor = "rgba(170, 170, 170, 1)"
                }
                console.log(HtmlImc[i])
            } 
        </script>
        <script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
</body>
</html>
<?php }else{
    header('location:./register-login.php');
}?>