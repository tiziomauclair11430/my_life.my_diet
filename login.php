<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="css/register.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400&display=swap" rel="stylesheet">
</head>
<body>
<header>
    <img src="stock/ress/logoDiet.png" class="logo-header">
    <a href="index.php" class="white">Home</a>
</header> 
    <label class="label-register label-login">Login :</label>
    <form action="./controller/login.php" method="post" class="login">
        <label><i class="fa-regular fa-user"></i>Username : </label>
        <input type="text" placeholder="8 - 20 character" name="username">
        <label><i class="fa-solid fa-lock"></i>Password : </label>
        <input type="password" name="password">
        <input type="submit" value="submit">
    </form>
    <a href="register-login.php" class="change">Register</a>
</body>
</html>