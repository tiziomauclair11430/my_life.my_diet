<?php
$host = 'localhost';
$db = 'myDiet';
$user = 'ti';
$pass = 'mauclair';

try{
$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
catch(PDOException $e){
    echo $e->getMessage()
;}

function addUser($username,$mail,$firstname,$lastname,$birth,$gender,$password){
    try{
      global $pdo;
        $req = $pdo->prepare('INSERT INTO utilisateur(pseudo,mail,firstname,lastname,birth,genre,mdp,date) VALUES (?,?,?,?,?,?,?,NOW())');
        $req->execute([$username,$mail,$firstname,$lastname,$birth,$gender,$password]);  
        return $pdo->lastInsertId();
    }catch(Exception $e){
        echo " Erreur ! ".$e->getMessage();
        echo $req;
    } 
}

function addPicture($picture,$id){
    try{
    global $pdo;
    $req = $pdo->prepare("UPDATE utilisateur SET photo_profil = ? WHERE id = ?");
    $req->execute([$picture,$id]);
    }catch(Exception $e){
    echo " Erreur ! ".$e->getMessage();
    echo $req;
    } 
}

function verif($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM liaison WHERE id_user = ?');
    $req->execute([$id]);
    $exist = $req->fetch();
    if ($exist) {
        return true;
    } else {
        return false;
    }
}

function SelectUser($id){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE id = ?');
    $req->execute([$id]);
    return $req->fetchAll();
}

function addInfo($imc,$cal,$weight,$height,$condition,$age){
    try{
        global $pdo;
          $req = $pdo->prepare('INSERT INTO info(imc,cal,weight,height,activite,age,date) VALUES (?,?,?,?,?,?,?)');
          $req->execute([$imc,$cal,$weight,$height,$condition,$age,date('Y-m-d')]);  
          return $pdo->lastInsertId();
      }catch(Exception $e){
          echo " Erreur ! ".$e->getMessage();
          echo $req;
      } 
}

function liaison($id_imc,$id_user){
    try{
        global $pdo;
          $req = $pdo->prepare('INSERT INTO liaison(id_imc,id_user) VALUES (?,?)');
          $req->execute([$id_imc,$id_user]);  
    }catch(Exception $e){
          echo " Erreur ! ".$e->getMessage();
          echo $req;
    }
}

function SelectAll($id){
    try{
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM liaison INNER JOIN info ON liaison.id_imc = info.id WHERE liaison.id_user = ? ORDER BY ID DESC LIMIT 1');
        $req->execute([$id]);
        return $req->fetchAll();
    }catch(Exception $e){
    echo " Erreur ! ".$e->getMessage();
    echo $req;
    }
}

function ClientExist($pseudo,$mdp){
    try{
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM utilisateur WHERE pseudo = ? AND mdp = ?');
        $req->execute([$pseudo,$mdp]);
        return $req->fetchAll();
    }catch(Exception $e){
    echo " Erreur ! ".$e->getMessage();
    echo $req;
    }
}

function UpdateUser($column,$rep,$id){
    try{
    global $pdo;
    $req = $pdo->prepare("UPDATE utilisateur SET ".$column." = ? WHERE id = ?");
    $req->execute([$rep,$id]);
    }catch(Exception $e){
    echo " Erreur ! ".$e->getMessage();
    echo $req;
    } 
}

function UpdateInfo($table,$column,$rep,$id){
    try{
    global $pdo;
    $req = $pdo->prepare("UPDATE liaison INNER JOIN info ON liaison.id_imc = info.id SET ".$table.".".$column." = ?  WHERE liaison.id_user = ? AND info.date = ?");
    $req->execute([$rep,$id,date('Y-m-d')]);
    }catch(Exception $e){
    echo " Erreur ! ".$e->getMessage();
    echo $req;
    } 
}

function CalculIMC($weight,$height){
    return $weight / pow($height,2);
}

function deleteClient($effacer){
    global $pdo;
    try{
        $req = $pdo->prepare('DELETE FROM utilisateur WHERE id = ?');
        $req->execute([$effacer]); 
    }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
    }
};
function deleteClientInfo($effacer){
    global $pdo;
    try{
        $req = $pdo->prepare('DELETE info,liaison FROM info INNER JOIN liaison ON info.id = liaison.id_imc WHERE liaison.id_user = ?');
        $req->execute([$effacer]); 
    }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
    }
};

function SelectAllInfo($id){
    try{
        global $pdo;
        $req = $pdo->prepare('SELECT * FROM liaison INNER JOIN info ON liaison.id_imc = info.id WHERE liaison.id_user = ?');
        $req->execute([$id]);
        return $req->fetchAll();
    }catch(Exception $e){
    echo " Erreur ! ".$e->getMessage();
    echo $req;
    }
}