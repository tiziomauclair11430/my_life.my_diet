DROP DATABASE IF EXISTS myDiet;
CREATE DATABASE myDiet;
USE myDiet;
CREATE TABLE utilisateur (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(120),
    mail VARCHAR(120),
    firstname VARCHAR(120),
    lastname VARCHAR(120),
    birth VARCHAR(255),
    genre boolean,
    mdp VARCHAR(270),
    photo_profil VARCHAR(255),
    date VARCHAR(255)
);

CREATE TABLE info (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    imc FLOAT,
    cal FLOAT,
    weight FLOAT,
    height FLOAT,
    activite FLOAT,
    age INT,
    date VARCHAR(255)
);

CREATE TABLE liaison (
    id_imc INT UNSIGNED,
    id_user INT UNSIGNED,
    UNIQUE(id_imc,id_user),
    FOREIGN KEY (id_imc) REFERENCES info(id) ON DELETE CASCADE,
    FOREIGN KEY (id_user) REFERENCES utilisateur(id) ON DELETE CASCADE
);

