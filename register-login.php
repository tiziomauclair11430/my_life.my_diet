<?php session_start();
var_dump();
if(isset($_SESSION['compte'])){
    header('location:./client.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="css/register.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <img src="stock/ress/logoDiet.png" class="logo-header">
        <a href="index.php" class="white">Home</a>
    </header>
    <div class="main">
        <label class="label-register">Register :</label>
        <form action="./controller/register.php" method="post" class="register">
            <label><i class="fa-regular fa-user"></i>Username : </label>
            <input type="text" placeholder="8 - 20 character" name="username">
            <label><i class="fa-solid fa-at"></i>Mail : </label>
            <input type="email" placeholder="Exemple@mail.com" name="email">
            <input type="text" placeholder="First name" name="fname">
            <input type="text" placeholder="Last name" name="lname">
            <input type="date" name="date" min="1920-01-01" max="2012-12-31" value="2000-01-01">
            <select name="gender">
                <option value="0">Homme</option>
                <option value="1">Femme</option>
            </select>
            <label><i class="fa-solid fa-lock"></i>Password : </label>
            <input type="password" name="password">
            <input type="submit" value="submit">
        </form>
        <a href="login.php" class="change">login</a>
    </div>
</body>
<script src="https://kit.fontawesome.com/7d1043cb42.js" crossorigin="anonymous"></script>
</html>
