<?php session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="./css/home.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <a href="register-login.php" class="white"><?php if(isset($_SESSION['compte'])){echo $_SESSION['compte']['pseudo'];}else{echo 'Connexion/Inscription';}?></a>
    </header>
    <div class="main">
    <img src="stock/ress/logoDiet.png" class="logo">
    <p>My life, my diet helps you eat a reasonable amount to keep your bmi normal</p>
    <p>we don't give any <strong class="blue">instructions</strong>, just <strong class="blue">statistics</strong>, it's up to you to follow them or not.</p>
    <img src="./stock/ress/images.jpeg">
    <p>Check your <strong class="blue">BMI</strong> and <strong class="blue">calorie amount</strong>, day by day on this web application</p>
    <p>BMI = weight in kg / height² in m. BMI can be calculated alone, or with a doctor. The WHO defines several alert thresholds</p>
    <img src="./stock/ress/salle-sport.jpg">
    <p>create a user account (it's free) to start using the <strong class="blue">My life, My diet</strong> application</p>
    <div class="two-img">
        <img src="./stock/ress/exemple2.png">
        <img src="./stock/ress/exemple1.png">
    </div>
    <a href="register-login.php" class="a-history white btn-create">Click Here to Create user account</a>
    <a href="login.php" class="a-history white btn-login">Click Here to Connect user account</a>
    </div>
    <footer>
        <p class="white">
        This application only saves the information given in the registration form and your BMI
        No data will be shared or sold to another third-party application or to a company
        be careful not to take the information given by the application as an absolute answer, some of our answers will not be 100% accurate
        </p>
    </footer>
</body>
</html>